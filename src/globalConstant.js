﻿module.exports = {

    RabbitMqServerAddress:      '192.168.0.18',
    RabbitMqServerPort:         5672,

    RabbitMqServerUsername:     'guest',
    RabbitMqServerPassword:     'guest',

    RabbitMqServerConnectionTimeOut: 10000,
    
    logger: 'logger',

    ExchangeType: 'direct',
    ExchangeDurable: false,
    ExchangeAutoDelete: true
};
